package main

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/baugoncalves/mentoria-test/calc"

	"github.com/gorilla/mux"
)

func helloWorld(w http.ResponseWriter, r *http.Request) {
	encoder := json.NewEncoder(w)
	encoder.Encode("Hello World")
}

func main() {
	var router *mux.Router

	log.Println("Server started on: https://localhost:1337")

	router = mux.NewRouter()
	router.HandleFunc("/", helloWorld)
	router.HandleFunc("/sum", calc.SumRoute)

	err := http.ListenAndServe(":1337", router)
	if err != nil {
		log.Println(err.Error())
	}

}
